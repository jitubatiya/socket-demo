const { Console } = require("console");
const express=require("express");
const { Socket } = require("net");
const app=express();
const server=require("http").createServer(app);
const io=require("socket.io").listen(server);
const port=3000;
io.on("connection",socket=>{
    console.log("a user connected :D"+socket.id);
    socket.on("chat message",msg=>{
        console.log(msg.msg);
        io.emit("chat message", msg);
    });
});

server.listen(port,()=>console.log("server running on port"+port));
