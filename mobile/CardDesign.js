import React, { Component } from 'react';
import {
    View,
    Text,
     Image,TouchableOpacity,StyleSheet
  } from 'react-native';
  export default class CardDesign extends React.Component{
      constructor(props)
      {
          super(props);
          this.state={
              data:this.props.item
          }
      }
      render(){
          const key=this.state.data.key;
          return(
              <View>
                  {key=="1"?
                <Text style={{textAlign:'right'}}>{this.state.data.msg}</Text>:
                <Text style={{textAlign:'left'}}>{this.state.data.msg}</Text>
                }
                </View>
            
          );
      }
  }
  const styles = StyleSheet.create({
    row: {
      flexDirection: 'row',
      alignItems: 'center',
      borderColor: '#dcdcdc',
      backgroundColor: '#fff',
      borderBottomWidth: 1,
      padding: 10,
      justifyContent: 'space-between',
  
    },
    pic: {
      borderRadius: 25,
      width: 50,
      height: 50,
    },
    nameContainer: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      width: 270,
    },
    nameTxt: {
      marginLeft: 15,
      fontWeight: '800',
      color: '#222',
      fontSize: 15,
  
    },
    
    end: {
      flexDirection: 'row',
      alignItems: 'center',
    },
    time: {
      fontWeight: '400',
      color: '#666',
      fontSize: 12,
  
    },
    icon:{
      height: 28,
      width: 28, 
    },
  });
  