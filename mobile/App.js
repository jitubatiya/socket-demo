import React, { Component } from "react";
import { TextInput, StyleSheet, Text, View,FlatList } from "react-native";
import io from "socket.io-client";
import CardDesign from "./CardDesign";
export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      chatMessage: "",
      chatMessages: []
    };
  }

  componentDidMount() {
    this.socket = io("http://192.168.43.121:3000");
    this.socket.on("chat message", msg => {
      this.setState({ chatMessages: [...this.state.chatMessages, msg] });
    });
  }

  submitChatMessage() {
    const data={"msg":this.state.chatMessage,"key":"1"};
    this.socket.emit("chat message",data);
    this.setState({ chatMessage: "" });
  }

  render() {


    return (
      <View style={styles.container}>
        <TextInput
          style={{ height: 40, borderWidth: 2 ,width:'100%'}}
          autoCorrect={false}
          value={this.state.chatMessage}
          onSubmitEditing={() => this.submitChatMessage()}
          onChangeText={chatMessage => {
            this.setState({ chatMessage });
          }}
        />
        <FlatList           
          extraData={this.state}
          data={this.state.chatMessages}
          keyExtractor = {(item) => {
            return item;
          }}
          renderItem={({item})=> <CardDesign item={item}/>}       
          />
      </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F5FCFF",
    flexDirection:'column'
  }
});